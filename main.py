from Qfx import *
import pprint
from operator import attrgetter
import timeit

start=timeit.default_timer()

popsize=1000
m=3

theta=0.0015
i=0
iteration=1000
data=np.genfromtxt('mcx4.csv', delimiter=",")
#print data
cities=len(data)

q=initialize(popsize,m, cities)

P=make(q)

pp=pprint.PrettyPrinter(indent=1)
#pp.pprint(P)

X=repair(P)
#pp.pprint(X)

calc(X,data)
#pp.pprint(X)

S=sorted(X, key=lambda X: X[1], reverse=True)
b=S[0]
print "b=", b

while i <= iteration:
	P=make(q)
	X=repair(P)
	calc(X, data)

	update(q, X, b, m, theta)

	S=sorted(X, key=lambda X: X[1], reverse=True)
	b=S[0]
	print "i=", i,"\t", "b=", b[1]
	
	i=i+1

stop=timeit.default_timer()

print "Time taken=", stop-start
