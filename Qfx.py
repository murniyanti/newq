import numpy as np, math, random
import copy

def initialize(popsize, m, cities):
	a=1/math.sqrt(2)
	b=1/math.sqrt(2)

	q=np.zeros([popsize,cities,2,m]).tolist()

	for p in range(popsize):
		for c in range(cities):
			for d in range(2):
				for b in range(m):
					q[p][c][d][b]=round(a,3)

	return q

def make(q):
	P=[]
	for p in range(len(q)):
		xsolution=[]
		for c in range(len(q[p])):
			bitP=[]
			for m in range(len(q[p][c][1])):
				if random.random() < math.pow(q[p][c][1][m],2):
					bitP.append(0)
				else:
					bitP.append(1)
			xsolution.append(copy.deepcopy(bitP))
		P.append(copy.deepcopy(xsolution))
	return P

def repair(P):

	X=[]

	for n in range(len(P)):
		bintodec=[]

		for c in range(len(P[n])):
			a="".join(str(y) for y in P[n][c])
			b=int(a,2)
			bintodec.append(copy.deepcopy(b))

		for k in range(len(bintodec)):

			if bintodec[k]>=len(P[n]):
				bintodec[k]=None

		for k in range(len(bintodec)):

			indexi=[y for y, x in enumerate(bintodec) if x==k]

			if len(indexi)>1:
				for h in range(1, len(indexi)):
					bintodec[indexi[h]]=None

		possibleCity=[]
		i=0

		while i<len(bintodec):
			if i not in bintodec:
				possibleCity.append(copy.deepcopy(i))
			i=i+1

		for k in range(len(bintodec)):
			if bintodec[k]==None:
				bintodec[k]=copy.deepcopy(random.choice(possibleCity))
				possibleCity.remove(bintodec[k])

		e=[bintodec,0]
		X.append(copy.deepcopy(e))

	return X


def calc(X, c):

	for p in range(len(X)):
		score_add=0

		for k in range(len(X[p][0]) - 1):
			#print X[p][k]
			a = c[ X[p][0][k]   ] [ X[p][0][k+1] ]

			b = c[ X[p][0][k+1] ] [ X[p][0][k]   ]

			if a >= b:
				score_add=score_add+a
			else:
				score_add=score_add+b

		score_add=score_add+X[p][0][k]

		X[p][1]=score_add



def update(q, X, b, m, theta):

	for p in range(len(q)):

		for c in range(len(q[p][0])):

			xs=np.binary_repr(X[p][0][c], m)
			bs=np.binary_repr(b[0][c], m)

			new_a=[]
			new_b=[]

			for k in range(len(q[p][c][0])):
				cxs=int(xs[k])
				cbs=int(bs[k])

				if X[p][1] <= b[1]:

					if cxs==0 and cbs==1:
						angle= -theta*math.pi 

					elif cxs==1 and cbs==0:
						angle= theta*math.pi 

					else:
						angle=random.uniform(0,0.005)*math.pi 
				else:
					angle=0

			rotateGate= [ [np.cos(angle), -1*np.sin(angle)], [-1*np.sin(angle), np.cos(angle) ] ]

			new_ab= np.dot( rotateGate, (q[p][c][0][k], q[p][c][1][k] ) ).tolist()

			new_a.append(copy.deepcopy(new_ab[0]))
			new_b.append(copy.deepcopy(new_ab[1]))

		q[p][c][0]=new_a
		q[p][c][1]=new_b


















